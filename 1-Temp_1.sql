alter database "Empresa" SET DATESTYLE TO 'DMY';

/* ------------------------------------------------------------------------------------ */
/* Crear tablas */

CREATE TABLE empleados_tv (
  nombre VARCHAR(20), 
  dni VARCHAR(9), 
  sueldo NUMERIC(5), 
  dn NUMERIC(2), 
  superdni VARCHAR(9),
  tiv DATE,
  tfv DATE,
  tit TIMESTAMP,
  tft TIMESTAMP,
  PRIMARY KEY (dni,tiv,tit)
);

/* ------------------------------------------------------------------------------------ */
  
  CREATE OR REPLACE FUNCTION VER() RETURNS INTEGER AS $$
  DECLARE
    tupla empleados_tv%ROWTYPE;
    cursor_ver_empleados CURSOR FOR SELECT * FROM empleados_tv;
    now DATE := '01-01-3000';
	uc TIMESTAMP :='01-01-4000';
  BEGIN
      RAISE NOTICE '--- VER EMPLEADOS ---';
      OPEN cursor_ver_empleados;
      LOOP
        FETCH cursor_ver_empleados INTO tupla;
        IF tupla IS NULL THEN EXIT;
        END IF;
        IF tupla.tfv=now AND tupla.tft = uc  THEN
          RAISE NOTICE '%  -  %  -  %  -  %  -  %  -  %  -  NOW -  %  -  UC',rpad(tupla.nombre,20,' '),rpad(tupla.dni,9,' '),to_char(tupla.sueldo,'99999'),to_char(tupla.dn,'99'),rpad(tupla.superdni,9,' '),tupla.tiv, tupla.tit;
        ELSE
          RAISE NOTICE '%  -  %  -  %  -  %  -  %  -  %  -  %  -  %  -  %',rpad(tupla.nombre,20,' '),rpad(tupla.dni,9,' '),to_char(tupla.sueldo,'99999'),to_char(tupla.dn,'99'),rpad(tupla.superdni,9,' '),tupla.tiv, tupla.tfv, tupla.tit, tupla.tft;
        END IF;
      
      END LOOP;
      CLOSE cursor_ver_empleados;
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;

/* ------------------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------------------ */
  
  CREATE OR REPLACE FUNCTION VER_OL() RETURNS INTEGER AS $$
  DECLARE
    tupla empleados_tv%ROWTYPE;
    cursor_ver_empleados CURSOR IS 
	SELECT * 
	FROM empleados_tv
	WHERE (tiv,tfv) OVERLAPS ('2003-01-01','3000-01-01');
    now DATE := '01-01-3000';
	uc TIMESTAMP :='01-01-4000';
  BEGIN
      RAISE NOTICE '--- VER OVERLAP ENTRE 2003 y 3000---';
      OPEN cursor_ver_empleados;
      LOOP
        FETCH cursor_ver_empleados INTO tupla;
        IF tupla IS NULL THEN EXIT;
        END IF;
        RAISE NOTICE '%  -  %  -  %  -  %  -  %  -  %  -  %  -  %  -  %',rpad(tupla.nombre,20,' '),rpad(tupla.dni,9,' '),to_char(tupla.sueldo,'99999'),to_char(tupla.dn,'99'),rpad(tupla.superdni,9,' '),tupla.tiv, tupla.tfv, tupla.tit, tupla.tft;
      END LOOP;
      CLOSE cursor_ver_empleados;
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;

/* ------------------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------------------ */
  
  CREATE OR REPLACE FUNCTION VER_BEFORE() RETURNS INTEGER AS $$
  DECLARE
    tupla empleados_tv%ROWTYPE;
	cursor_ver_empleados CURSOR IS 
    SELECT * 
	FROM empleados_tv
	WHERE tiv <'01-01-2002';

  BEGIN
      RAISE NOTICE '--- VER EMPLEADOS CON TIV PREVIO A 2002 ---';
      OPEN cursor_ver_empleados;
      LOOP
        FETCH cursor_ver_empleados INTO tupla;
        IF tupla IS NULL THEN EXIT;
        END IF;

        RAISE NOTICE '%  -  %  -  %  -  %  -  %  -  %  -  %  -  %  -  %',rpad(tupla.nombre,20,' '),rpad(tupla.dni,9,' '),to_char(tupla.sueldo,'99999'),to_char(tupla.dn,'99'),rpad(tupla.superdni,9,' '),tupla.tiv, tupla.tfv, tupla.tit, tupla.tft;

      
      END LOOP;
      CLOSE cursor_ver_empleados;
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;

/* ------------------------------------------------------------------------------------ */

/* ------------------------------------------------------------------------------------ */
  
  CREATE OR REPLACE FUNCTION VER_EXAMEN(pdate IN DATE) RETURNS INTEGER AS $$
  DECLARE
	tupla empleados_tv%ROWTYPE;
	
    tupla_ant empleados_tv%ROWTYPE;
	
	cursor_ver_empleados_ant CURSOR IS 
    SELECT * 
	FROM empleados_tv
	WHERE tiv <= ver_examen.pdate;
	
	tupla_post empleados_tv%ROWTYPE;
	
	cursor_ver_empleados_post CURSOR IS 
    SELECT * 
	FROM empleados_tv
	WHERE tiv >=ver_examen.pdate;

  BEGIN
     
	  OPEN cursor_ver_empleados_ant;
	  OPEN cursor_ver_empleados_post;
	  
	  SELECT * INTO tupla_ant
	  FROM empleados_tv
	  WHERE tiv<=ver_examen.pdate
	  LIMIT 1;
	  
	  SELECT * INTO tupla_post
	  FROM empleados_tv
	  WHERE tiv>=ver_examen.pdate
	  LIMIT 1;
	  
	  
	  RAISE NOTICE '--- EMPLEADO ANTERIOR A 1-JULIO-2003 ---';
	  
	  LOOP
        FETCH cursor_ver_empleados_ant INTO tupla;
        IF tupla IS NULL THEN EXIT;
        END IF;
		IF tupla.tiv >= tupla_ant.tiv THEN
			tupla_ant := tupla;
		END IF;
      END LOOP;
	  
	  RAISE NOTICE '%  -  %  -  %  -  %  -  %  -  %  -  %  -  %  -  %',rpad(tupla_ant.nombre,20,' '),rpad(tupla_ant.dni,9,' '),to_char(tupla_ant.sueldo,'99999'),to_char(tupla_ant.dn,'99'),rpad(tupla_ant.superdni,9,' '),tupla_ant.tiv, tupla_ant.tfv, tupla_ant.tit, tupla_ant.tft;
    
	  RAISE NOTICE '--- EMPLEADO POSTERIOR A 1-JULIO-2003 ---';
       LOOP
        FETCH cursor_ver_empleados_post INTO tupla;
        IF tupla IS NULL THEN EXIT;
        END IF;
		IF tupla.tiv <= tupla_post.tiv THEN
			tupla_post := tupla;
		END IF;
      END LOOP;
	  
	  RAISE NOTICE '%  -  %  -  %  -  %  -  %  -  %  -  %  -  %  -  %',rpad(tupla_post.nombre,20,' '),rpad(tupla_post.dni,9,' '),to_char(tupla_post.sueldo,'99999'),to_char(tupla_post.dn,'99'),rpad(tupla_post.superdni,9,' '),tupla_post.tiv, tupla_post.tfv, tupla_post.tit, tupla_post.tft;
	CLOSE cursor_ver_empleados_ant;
	CLOSE cursor_ver_empleados_post;
	RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;

/* ------------------------------------------------------------------------------------ */

  CREATE OR REPLACE FUNCTION insertar (pnombre IN VARCHAR, pdni IN VARCHAR, psueldo IN NUMERIC, pdn IN NUMERIC, psuperdni IN VARCHAR, ptiv IN DATE, ptit IN TIMESTAMP) RETURNS INTEGER AS $$
  DECLARE
    now DATE := '01-01-3000';
	uc TIMESTAMP :='01-01-4000';
  BEGIN
    /* Se insertan los par�metros, con tfv como now y tft como uc */
    INSERT INTO empleados_tv
    VALUES (insertar.pnombre, insertar.pdni, insertar.psueldo, insertar.pdn, insertar.psuperdni, insertar.ptiv, now, insertar.ptit, uc);
    RAISE NOTICE 'INSERTADO: %',insertar.pnombre; 
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;

/* ------------------------------------------------------------------------------------ */

  CREATE OR REPLACE FUNCTION modificar (pnombre IN VARCHAR, pdni IN VARCHAR, psueldo IN NUMERIC, pdn IN NUMERIC, psuperdni IN VARCHAR, pfecha IN DATE, ptit IN TIMESTAMP) RETURNS INTEGER AS $$
  DECLARE
    tiv_inicial empleados_tv.tiv%TYPE;
    tupla empleados_tv%ROWTYPE;
    cursor_modificar CURSOR IS 
       SELECT *
       FROM empleados_tv
       WHERE dni=pdni;
    now DATE := '3000-1-1';
	uc TIMESTAMP :='01-01-4000';
  BEGIN
    /* Recuperar todas las tuplas del empleado 'pdni' y recuperar el 'tiv' de la tupla actual */
    OPEN cursor_modificar;
    LOOP
      FETCH cursor_modificar INTO tupla;
      IF tupla IS NULL THEN EXIT;
      END IF;
      IF tupla.tfv=now AND tupla.tft=uc THEN
        tiv_inicial:=tupla.tiv;
		 /* Cerrar la tupla actual (ya tenemos el 'tiv' de la tupla actual). tft=ptit, tfv = fecha*/
		UPDATE empleados_tv
		SET tfv=pfecha, tft=ptit
		WHERE dni=pdni and tiv=tiv_inicial;
		/*Crear V2. TFV es nueva fecha, tit es el introducido, tft es uc*/
		INSERT INTO empleados_tv
		VALUES (tupla.nombre, tupla.dni, tupla.sueldo, tupla.dn, tupla.superdni, tupla.tiv, pfecha, modificar.ptit, uc);
      END IF;
    END LOOP;
	CLOSE cursor_modificar;

   

    /* Actualizar la fecha: tiv de la tupla nueva debe ser al d�a siguiente*/
    pfecha=pfecha+1;
    
    /* Crear V3. tiv es pfecha+1, tft = uc, tfv = now, tit = ptit */
    INSERT INTO empleados_tv
    VALUES (modificar.pnombre, modificar.pdni, modificar.psueldo, modificar.pdn, modificar.psuperdni, pfecha, now , modificar.ptit, uc);

    RAISE NOTICE 'MODIFICADO: %',modificar.pnombre;
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;

/* ------------------------------------------------------------------------------------ */

  CREATE OR REPLACE FUNCTION borrar (pdni IN VARCHAR, ptiv IN DATE, ptfv IN DATE ,ptit IN TIMESTAMP) RETURNS INTEGER AS $$
  DECLARE
    pnombre empleados_tv.nombre%TYPE;
	tupla empleados_tv%ROWTYPE;
	v2 empleados_tv%ROWTYPE;
    cursor_borrar CURSOR IS 
       SELECT *
       FROM empleados_tv
       WHERE dni=pdni;
	now DATE := '3000-1-1';
	uc TIMESTAMP :='01-01-4000';
  BEGIN
	
	OPEN cursor_borrar;
    LOOP
      FETCH cursor_borrar INTO tupla;
      IF tupla IS NULL THEN EXIT;
      END IF;
      IF tupla.tft=uc THEN
		IF tupla.tfv=now THEN
		v2 := tupla;
		END IF;
      END IF;
    END LOOP;
	CLOSE cursor_borrar;

	  /* Cerrar la tupla */    
    UPDATE empleados_tv
    SET tft=ptit
    WHERE dni=pdni and tiv=ptiv;
	
	 /*Insertamos V2*/
		INSERT INTO empleados_tv
		VALUES (v2.nombre, v2.dni, v2.sueldo, v2.dn, v2.superdni, v2.tiv, borrar.ptfv, borrar.ptit, uc);
  

    SELECT nombre INTO pnombre
    FROM empleados_tv
    WHERE dni=pdni and tiv=ptiv;

    RAISE NOTICE 'BORRADO: %',pnombre;
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;

/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */

  DELETE FROM EMPLEADOS_TV;

  /* Insertar */
  SELECT insertar('Perez', '123456789', 25000, 5, '333445555', '15-06-2002', '08-06-2002 13:05:58');
  SELECT insertar('Campos', '333445555' ,25000, 4, '444444444', '20-08-1999', '20-08-1999 11:18:23');
  SELECT insertar('Torres', '222447777', 25000, 4, '111245555', '01-05-2001', '27-04-2001 16:22:05');
  SELECT insertar('Ojeda', '666884444', 25000, 5, '333445555', '01-08-2003', '28-07-2003 09:25:37');
  SELECT insertar('Ruiz', '666999333', 25000, 5, '333445555', '01-07-2002', '01-01-2015 12:00:15');
  SELECT insertar('Jimenez', '987654321', 25000, 4, '111245555','01-10-2003', '01-01-2015 15:16:30');
  SELECT ver();

  /* Modificar */
  SELECT modificar('Perez', '123456789', 30000, 5, '333445555','31-05-2003', '04-06-2003 08:56:12');
  SELECT ver();
  SELECT modificar('Campos', '333445555' ,30000, 5, '444444444','31-01-2001', '07-01-2001 14:33:02');
  SELECT ver();
  SELECT modificar('Campos', '333445555' ,40000, 5, '444444444','31-03-2002', '28-03-2002 09:23:57');
  SELECT ver();

  /* Borrar */
  SELECT borrar('222447777','01-05-2001', '10-08-2002', '12-08-2002 10:11:07'); /* Torres */
  SELECT ver();
  SELECT ver_ol();
  SELECT ver_before();
  SELECT ver_examen('01-07-2003');

  /* Borrar funciones */
  DROP FUNCTION ver();
  DROP FUNCTION ver_ol();
  DROP FUNCTION ver_before();
  DROP FUNCTION ver_examen(DATE);
  DROP FUNCTION insertar(VARCHAR, VARCHAR, NUMERIC, NUMERIC, VARCHAR, DATE, TIMESTAMP);
  DROP FUNCTION modificar(VARCHAR, VARCHAR, NUMERIC, NUMERIC, VARCHAR, DATE, TIMESTAMP);
  DROP FUNCTION borrar(VARCHAR, DATE, DATE, TIMESTAMP);

  /* Borrar tablas */
  DROP TABLE empleados_tv;

