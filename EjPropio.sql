alter database "Empresa" SET DATESTYLE TO 'DMY';
/* Creaci�n de la tabla armas */

CREATE TABLE armas (
  id NUMERIC(5),
  nombre VARCHAR(20), 
  danio NUMERIC(5), 
  bonus_fuerza NUMERIC(5),
  bonus_destreza NUMERIC(5),
  bonus_inteligencia NUMERIC(5),
  tiv DATE,
  tfv DATE,
  tit TIMESTAMP,
  tft TIMESTAMP,
  PRIMARY KEY (id, tiv, tit)
  );
  
CREATE OR REPLACE FUNCTION VER_ARMAS() RETURNS INTEGER AS $$
DECLARE
  tupla armas%ROWTYPE;
  cursor_ver_armas CURSOR FOR SELECT * FROM armas ORDER BY id;
    now DATE := '01-01-3000';
	uc TIMESTAMP :='01-01-4000';
BEGIN
    RAISE NOTICE '--- VER ARMAS ---';
    OPEN cursor_ver_armas;
    LOOP
      FETCH cursor_ver_armas INTO tupla;
      IF tupla IS NULL THEN EXIT;
      END IF;
	  IF tupla.tfv=now AND tupla.tft = uc  THEN
           RAISE NOTICE '%  -  %  -  %  -  %  -  %  -  %  -  %  -  NOW  -  %  -  UC',to_char(tupla.id,'99999'),rpad(tupla.nombre,20,' '),to_char(tupla.danio,'99999'),to_char(tupla.bonus_fuerza,'99999'),to_char(tupla.bonus_destreza,'99999'),to_char(tupla.bonus_inteligencia,'99999'),tupla.tiv,tupla.tit;
        ELSE
           RAISE NOTICE '%  -  %  -  %  -  %  -  %  -  %  -  %  -  %  -  %  -  %',to_char(tupla.id,'99999'),rpad(tupla.nombre,20,' '),to_char(tupla.danio,'99999'),to_char(tupla.bonus_fuerza,'99999'),to_char(tupla.bonus_destreza,'99999'),to_char(tupla.bonus_inteligencia,'99999'),tupla.tiv,tupla.tfv,tupla.tit,tupla.tft;
        END IF;
      
    END LOOP;
    CLOSE cursor_ver_armas;
  RETURN NULL;
END;
$$ LANGUAGE plpgsql;

/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */

CREATE OR REPLACE FUNCTION insertar (pid IN NUMERIC ,pnombre IN VARCHAR,pdanio IN NUMERIC, pbonusfuerza IN NUMERIC, pbonusdestreza IN NUMERIC, pbonusinteligencia IN NUMERIC, ptiv IN DATE, ptit IN TIMESTAMP) RETURNS INTEGER AS $$
  DECLARE
    now DATE := '01-01-3000';
	uc TIMESTAMP :='01-01-4000';
  BEGIN
    /* Se insertan los par�metros, con tfv como now y tft como uc */
    INSERT INTO armas
    VALUES (insertar.pid, insertar.pnombre, insertar.pdanio, insertar.pbonusfuerza, insertar.pbonusdestreza,insertar.pbonusinteligencia, insertar.ptiv, now, insertar.ptit, uc);
    RAISE NOTICE 'INSERTADO: %',insertar.pnombre; 
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;
  
  /* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */

CREATE OR REPLACE FUNCTION modificar (pid IN NUMERIC ,pnombre IN VARCHAR,pdanio IN NUMERIC, pbonusfuerza IN NUMERIC, pbonusdestreza IN NUMERIC, pbonusinteligencia IN NUMERIC, pfecha IN DATE, ptit IN TIMESTAMP) RETURNS INTEGER AS $$
  DECLARE
    tiv_inicial armas.tiv%TYPE;
    tupla armas%ROWTYPE;
    cursor_modificar CURSOR IS 
       SELECT *
       FROM armas
       WHERE id=pid;
    now DATE := '3000-1-1';
	uc TIMESTAMP :='01-01-4000';
  BEGIN
    /* Recuperar todas las tuplas del empleado 'pdni' y recuperar el 'tiv' de la tupla actual */
    OPEN cursor_modificar;
    LOOP
      FETCH cursor_modificar INTO tupla;
      IF tupla IS NULL THEN EXIT;
      END IF;
      IF tupla.tfv=now AND tupla.tft=uc THEN
        tiv_inicial:=tupla.tiv;
		 /* Cerrar la tupla actual (ya tenemos el 'tiv' de la tupla actual). tft=ptit, tfv = fecha*/
		UPDATE armas
		SET tfv=pfecha, tft=ptit
		WHERE id=pid and tiv=tiv_inicial;
		/*Crear V2. TFV es nueva fecha, tit es el introducido, tft es uc*/
		INSERT INTO armas
		VALUES (tupla.id, tupla.nombre, tupla.danio, tupla.bonus_fuerza, tupla.bonus_destreza, tupla.bonus_inteligencia, tupla.tiv, pfecha, modificar.ptit, uc);
      END IF;
    END LOOP;
	CLOSE cursor_modificar;

   

    /* Actualizar la fecha: tiv de la tupla nueva debe ser al d�a siguiente*/
    pfecha=pfecha+1;
    
    /* Crear V3. tiv es pfecha+1, tft = uc, tfv = now, tit = ptit */
    INSERT INTO armas
    VALUES (modificar.pid, modificar.pnombre,modificar.pdanio,modificar.pbonusfuerza,modificar.pbonusdestreza,modificar.pbonusinteligencia, pfecha, now , modificar.ptit, uc);

    RAISE NOTICE 'MODIFICADO: %',modificar.pnombre;
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;
  
  /* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */
  
  CREATE OR REPLACE FUNCTION borrar (pid IN NUMERIC, ptiv IN DATE, ptfv IN DATE ,ptit IN TIMESTAMP) RETURNS INTEGER AS $$
  DECLARE
	tupla armas%ROWTYPE;
	v2 armas%ROWTYPE;
    cursor_borrar CURSOR IS 
       SELECT *
       FROM armas
       WHERE id=pid;
	now DATE := '3000-1-1';
	uc TIMESTAMP :='01-01-4000';
  BEGIN
	
	OPEN cursor_borrar;
    LOOP
      FETCH cursor_borrar INTO tupla;
      IF tupla IS NULL THEN EXIT;
      END IF;
      IF tupla.tft=uc THEN
		IF tupla.tfv=now THEN
		v2 := tupla;
		END IF;
      END IF;
    END LOOP;
	CLOSE cursor_borrar;

	  /* Cerrar la tupla */    
    UPDATE armas
    SET tft=ptit
    WHERE id=pid and tiv=ptiv;
	
	 /*Insertamos V2*/
		INSERT INTO armas
		VALUES (v2.id,v2.nombre,v2.danio,v2.bonus_fuerza,v2.bonus_destreza,v2.bonus_inteligencia, v2.tiv, borrar.ptfv, borrar.ptit, uc);
  



    RAISE NOTICE 'BORRADO: %',v2.nombre;
    RETURN NULL;
  END;
  $$ LANGUAGE plpgsql;
  
  /* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */
/* ------------------------------------------------------------------------------------ */


DELETE FROM ARMAS;

SELECT insertar(1,'Hacha de la cosecha',3,1,2,3, '21-11-1999', '22-5-2000');
SELECT insertar(2,'Espada del viento',3,2,2,6, '21-10-1999', '12-5-2000');
SELECT insertar(3,'Mangual de amasar',3,2,2,6, '01-11-1999', '22-7-2000');
SELECT insertar(4,'Canto rodado',1337,1,1,1, '01-11-1942', '22-7-3000');

SELECT ver_armas();


SELECT modificar(1,'Hacha modificada',1,1,1,1,'01-01-2001','02-02-2002');

SELECT ver_armas();

SELECT borrar(3,'01-01-2001','03-03-2001','04-04-2001');

SELECT ver_armas();


DROP FUNCTION ver_armas();
DROP FUNCTION insertar(NUMERIC,VARCHAR,NUMERIC,NUMERIC,NUMERIC,NUMERIC,DATE,TIMESTAMP);
DROP FUNCTION modificar(NUMERIC ,VARCHAR,NUMERIC,NUMERIC,NUMERIC,NUMERIC,DATE,TIMESTAMP);
DROP FUNCTION borrar(NUMERIC,DATE,DATE ,TIMESTAMP);

DROP TABLE armas;